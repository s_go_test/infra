#default user on servers
variable "user" {
  type        = string
  default     = "ubuntu"
}

#path to public ssh key
variable "ssh_key_public" {
  default     = "~/.ssh/id_rsa.pub"
  description = "Path to the SSH public key for accessing cloud instances. Used for creating AWS keypair."
}

#path to provate ssh key
variable "ssh_key_private" {
  default     = "~/.ssh/id_rsa"
  description = "Path to the SSH public key for accessing cloud instances. Used for creating AWS keypair."
}

# ip port for access website on load balancer and work servers
variable "web_port" {
  type        = number
  default     = 8080 
}

#token gitlab runner
variable "RUN_ID" {
  type        = string
}