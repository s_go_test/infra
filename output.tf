#don't use in this version
#data "yandex_compute_image" "my_image" {
#  family         = "ubuntu-2004-lts"
#}

output "ip_lb" {
  value           = (yandex_lb_network_load_balancer.nw_balancer.listener[*].external_address_spec[*].address)[0][0]
}

output "ip_test" {
  value           = module.test.public_ip
}

output "ip_dev" {
  value           = module.dev.public_ip
}

output "ip_prod" {
  value           = module.prod.public_ip
}

output "ip_grafana" {
  value           = module.grafana.public_ip
}

output "ip_elk" {
  value           = module.elk.public_ip_elk
}

output "ip_nexus" {
  value           = module.nexus.public_ip_nexus
}

#don't use in this version
# resource "local_file" "ansible_inventory" {
#   content       = templatefile(
#     "hosts.tftpl",
#     { 
#       user      = "ubuntu"
#       prod      = [module.prod.public_ip]
#       dev       = [module.dev.public_ip]
#       test      = [module.test.public_ip]
#
#    }
# )
# filename        = "hosts"
#}

#don't use in this version
#resource "local_file" "serverip" {
#    content      = local.ip_host
#    filename     = "hosts"
#}