resource "yandex_vpc_network" "network-1" {
  name                = "network1"
}

#create network load balancer
resource "yandex_lb_network_load_balancer" "nw_balancer" {
  name                = "my-network-load-balancer"
  listener {
    name              = "my-listener"
    port              = var.web_port
    external_address_spec {
      ip_version      = "ipv4"
    }
}

  attached_target_group {
    target_group_id   = yandex_lb_target_group.web-servers.id
 
    healthcheck {
      name            = "http"
      http_options {
        port          = var.web_port
        path          = "/"
      }
    }
  }
}

#add servers to load balancer
resource "yandex_lb_target_group" "web-servers" {
  name                = "web-servers-target-group" 
  target {
    subnet_id         = module.prod.vpc_subnet_id
    address           = module.prod.ip_address
  }
}


#create dns zone
resource "yandex_dns_zone" "zone1" {
  name                = "my-zone"
  description         = "test-kralin.ga"
  zone                = "test-kralin.ga."
  public              = true
}


#add dns for load balanser
resource "yandex_dns_recordset" "dns_balanser" {
  zone_id             = yandex_dns_zone.zone1.id
  name                = "lb.test-kralin.ga."
  type                = "A"
  ttl                 = 200
  data                = [(yandex_lb_network_load_balancer.nw_balancer.listener[*].external_address_spec[*].address)[0][0]]
  depends_on = [
    yandex_lb_network_load_balancer.nw_balancer
  ]
}
