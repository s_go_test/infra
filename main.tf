terraform {
  required_providers {
    yandex = {
    source          = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
}

#create server for developers
module "dev" {
  source            = "./modules/serv"
  vpc_network_id    = yandex_vpc_network.network-1.id
  dns_zone_id       = yandex_dns_zone.zone1.id
  env               = "dev"
  ipv4              = "192.168.10.0/24"
  dns_name          = "dev.test-kralin.ga."
  ssh_key_public    = var.ssh_key_public
  ssh_key_private   = var.ssh_key_private
  RUN_ID            = var.RUN_ID
}

#create server for test
module "test" {
  source            = "./modules/serv"
  vpc_network_id    = yandex_vpc_network.network-1.id
  dns_zone_id       = yandex_dns_zone.zone1.id
  env               = "test"
  ipv4              = "192.168.20.0/24"
  dns_name          = "test.test-kralin.ga."
  ssh_key_public    = var.ssh_key_public
  ssh_key_private   = var.ssh_key_private
  RUN_ID            = var.RUN_ID
}

#create server for prod
module "prod" {
  source            = "./modules/serv"
  vpc_network_id    = yandex_vpc_network.network-1.id
  dns_zone_id       = yandex_dns_zone.zone1.id
  env               = "prod"
  ipv4              = "192.168.30.0/24"
  dns_name          = "prod.test-kralin.ga."
  ssh_key_public    = var.ssh_key_public
  ssh_key_private   = var.ssh_key_private
  RUN_ID            = var.RUN_ID
}

#create server for grafana
module "grafana" {
  source            = "./modules/grafana"
  vpc_network_id    = yandex_vpc_network.network-1.id
  dns_zone_id       = yandex_dns_zone.zone1.id
  ssh_key_public    = var.ssh_key_public
  ssh_key_private   = var.ssh_key_private
}

#create server for elk
module "elk" {
  source            = "./modules/elk"
  vpc_network_id    = yandex_vpc_network.network-1.id
  dns_zone_id       = yandex_dns_zone.zone1.id
  ssh_key_public    = var.ssh_key_public
  ssh_key_private   = var.ssh_key_private
}

#create server for nexus
module "nexus" {
  source            = "./modules/nexus"
  vpc_network_id    = yandex_vpc_network.network-1.id
  dns_zone_id       = yandex_dns_zone.zone1.id
  ssh_key_public    = var.ssh_key_public
  ssh_key_private   = var.ssh_key_private
}