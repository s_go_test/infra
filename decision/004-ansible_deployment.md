# Ansible deployment infrastructure
Date: 2022-09-14

## Status 
Accepted

## Context
We need deployment infrastructure in virtual machine

## Decision
We are using ansible to install docker and gitlab runner on servers

## Consequences
Install docker and gitlab runner on out virtual servers