# Terraform create module structure
Date: 2022-09-14

## Status 
Accepted

## Context
We need create in terraform module structure for servers and grafana

## Decision
We need to rebuild out terraform structure with module. We need create module for create grafana server and module for creating servers

## Consequences
Creating 2 module:
- grafana - up server with grafana
- serv - up server for out application (test, dev, prod or any other) 