# Install prometheus, node exporter and blackbox exporter in ansbile playbook
Date: 2022-09-14

## Status 
Accepted

## Context
We need install prometheus, node exporter and blackbox exporter on our servers

## Decision
Use ansible to install prometheus, node exporter and blackbox exporter
Create role for install prometheus, node exporter and blackbox exporter

## Consequences
Change ansible playbook configure.yml, and now he can install prometheus, node exporter and blackbox exporter