# Import datasouse and dashboard to grafana
Date: 2022-09-14

## Status 
Accepted

## Context
We need when create infrastructure, grafana have standart dashboard

## Decision
We use ansible  for import json to grafana
Creating 2 sh for import
import_datasource.sh fo import data source
import_dashboard.sh for import dashboard

## Consequences
With creating infrastructure configuring grafana dashboard