# Create grafana dashboard
Date: 2022-09-14

## Status 
Accepted

## Context
We need to monitoring our servers in grafana

## Decision
We need create data source and dashboard in grafana
In dashboard we need monitoring status of our site, usage CPU, Memory, disk and network statistics
for copying data source use 
'''
mkdir -p data_sources && curl -s "http://localhost:3000/api/datasources"  -u admin:admin | jq -c -M '.[]'|split -l 1 - data_sources/
'''
Copy dashboard in interface
    Open your Dasboard and click Share Dasboard button on top.
    Go to Export Tab and click Save to File.

## Consequences
We upload data source and dashboard in json format
