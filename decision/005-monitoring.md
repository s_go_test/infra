# Choose monitoring system
Date: 2022-09-14

## Status 
Accepted

## Context
We need add monitoring for our servers

## Decision
We choose grafana, node exporter, blackbox exporter and prometheus for monitoring

## Consequences
We will be able to collect metrics from our servers and display them in beautiful graphs