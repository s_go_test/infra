# Hosting Platforms
Date: 2022-09-14

## Status 
Accepted

## Context
We need to decide upon a platform to host the future infrastructure

## Decision
We are using Yandex cloud as our hosting provider of choice. We will initially be using the ru-central1-a region

## Consequences
We will create our infrastructure in hosting