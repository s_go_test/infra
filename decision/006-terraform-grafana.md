# Add grafana server in terraform
Date: 2022-09-14

## Status 
Accepted

## Context
We need  create grafana server on out infrastructure


## Decision
We create in terraform server for grafana and register domain name

## Consequences
We can connect to grafana use http://grafana.test-kralin.ga:3000