#!/bin/bash
grafana_host="http://localhost:3000"
grafana_cred="admin:admin"
grafana_datasource="serv-0"
DASH_DIR="/tmp/script/dashboard"

for i in $DASH_DIR/*; do \
    echo $i
    curl -X "POST" "$grafana_host/api/dashboards/db" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        --user $grafana_cred \
        -d "{\"dashboard\":$(cat $i),\"folderId\": 0,\"overwrite\": false}"
done