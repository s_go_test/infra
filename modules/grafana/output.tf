locals {
  ip_grafana    = (yandex_compute_instance.grafana.network_interface[*].nat_ip_address)[0]
}

output "public_ip" {
  value         = local.ip_grafana
}