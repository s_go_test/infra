locals {
  ip_elk      = (yandex_compute_instance.elk.network_interface[*].nat_ip_address)[0]
}

output "public_ip_elk" {
  value       = local.ip_elk
}