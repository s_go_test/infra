locals {
  ip_nexus      = (yandex_compute_instance.nexus.network_interface[*].nat_ip_address)[0]
}

output "public_ip_nexus" {
  value       = local.ip_nexus
}