locals {
  ip_serv = (yandex_compute_instance.serv.network_interface[*].nat_ip_address)[0]
}

output "public_ip" {
  value   = local.ip_serv
}

output "ip_address" {
  value   = yandex_compute_instance.serv.network_interface.0.ip_address
}

output "vpc_subnet_id" {
  value   = yandex_vpc_subnet.subnet_serv.id
}