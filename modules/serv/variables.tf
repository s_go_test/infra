#default user for server
variable "user" {
  type      = string
  default   = "ubuntu"
}

#name of instanse and gitlab runner tags
variable "env" {
  type      = string
  default   = "test"
}

#public ip port for website
variable "web_port" {
  type      = number
  default   = 8080 
}

#private network
variable "ipv4" {
  type      = string
  default   = "192.168.10.0/24"
}

#zone
variable "zone" {
  type      = string
  default   = "ru-central1-a"
}

#public dns name
variable "dns_name" {
  type      = string
  default   = "test.test-kralin.ga."
}

#external vpc_network_id
variable "vpc_network_id" {
  description = "vpc_network_id"
}

#external dns zone id
variable "dns_zone_id" {
  description = "dns_zone_id"
}

#public ssh key
variable "ssh_key_public" {
  description = "Path to the SSH public key for accessing cloud instances."
  default     = "~/.ssh/id_rsa.pub"
}

#private ssh key
variable "ssh_key_private" {
  description = "Path to the SSH public key for accessing cloud instances"
  default     = "~/.ssh/id_rsa"
}

#external gitlab runner token
variable "RUN_ID" {
  type      = string
}