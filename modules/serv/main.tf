terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_vpc_subnet" "subnet_serv" {
  zone                        = var.zone
  network_id                  = var.vpc_network_id
  v4_cidr_blocks              = [var.ipv4]
  description                 = "${var.env}-private"
}

#create dns name
resource "yandex_dns_recordset" "dns_serv" {
  zone_id                     = var.dns_zone_id
  name                        = var.dns_name
  type                        = "A"
  ttl                         = 200
  data                        = [(yandex_compute_instance.serv.network_interface[*].nat_ip_address)[0]]
}

#create instance
resource "yandex_compute_instance" "serv" {
  name                        = var.env
  allow_stopping_for_update   = true
  
  resources {
    cores                     = 2
    core_fraction             = 20
    memory                    = 2
  }

  boot_disk {
    initialize_params {
      #image_id = "${data.yandex_compute_image.my_image.id}"
      #ubuntu 20.04
      image_id                = "fd879gb88170to70d38a"
      size                    = 10
    }
  }

  network_interface {
    subnet_id                 = yandex_vpc_subnet.subnet_serv.id
    nat                       = true
  }

  #save money
  scheduling_policy {
    preemptible               = true
  }

  metadata = {
    ssh-keys                  = "${var.user}:${file(var.ssh_key_public)}"
  }

  provisioner "remote-exec" {
    inline                    = ["pwd"]

    connection {
      type                    = "ssh"
      host                    = (self.network_interface[*].nat_ip_address)[0]
      user                    = var.user
      private_key             = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ${var.user} -i '${(self.network_interface[*].nat_ip_address)[0]},' --private-key ${var.ssh_key_private} ${path.module}/configure.yml --extra-vars '{\"REGISTRATION_TOKEN\":\"${var.RUN_ID}\", \"stand\":\"${var.env}\"}'"
  }

  lifecycle {
    create_before_destroy     = true
  } 

}
