# infra

## load balancer:  lb.test-kralin.ga
http://lb.test-kralin.ga:8080

## dev server:     dev.test-kralin.ga
http://dev.test-kralin.ga:8080

## test server:    test.test-kralin.ga
http://test.test-kralin.ga:8080

## grafana: grafana.test-kralin.ga
http://grafana.test-kralin.ga:3000/login

## elk: elk.test-kralin.ga
http://elk.test-kralin.ga:5601/

## nexus nexus.test-kralin.ga
http://nexus.test-kralin.ga

## Start
# for create the infrastructure do the following:
set your  data for cloud and export 
- export YC_TOKEN=token
- export YC_CLOUD_ID=cloud_id
- export YC_FOLDER_ID=folder_id
- export YC_ZONE=ru-central1-a
- export TF_VAR_RUN_ID=token gitlab-runner

## and run:
- terraform init
- terraform apply